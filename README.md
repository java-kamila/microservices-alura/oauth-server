# Oauth Server

<img src="autenticacao.png" />

 - Quando o usuário faz uma requisição para um serviço que demanda que o usuário esteja logado, no header da requisição, o usuário envia um token, que ele conseguiu previamente através do servidor de autenticação.
 - A integração do **Spring Security** e o **Spring Cloud OAuth** é feita quanto estendemos o WebSecurityConfigurerAdapter e expomos os recursos do Spring Security para serem utilizados pelo Spring Cloud OAuth.
 - `AuthenticationManager` e `UserDetailService` são os beans expostos do `Spring Security` e injetados no Adapter do Spring Cloud OAuth2: o `AuthorizationServerConfigurerAdapter`. Mais especificamente, a integração é feita no método configure deste adapter.
 
##### Requests

 - Autoriza usuário
 
<img src="request_auth1.png">

<img src="request_auth2.png">

 - Busca dados de autenticação de um usuário
 
<img src="valida_usuario.png">